<?php
declare(strict_types=1);

return [
    'routes'=>[
        /**
         * Управление доступом
         */
        [
            'method'=>'GET',
            'pattern'=>'v1/everydays>',
            'controller'=>\web\v1\main\MainController::class,
            'action'=>'actionGetEveryday'
        ],
        [
            'method'=>'GET',
            'pattern'=>'v1/access>',
            'controller'=>\web\v1\main\MainController::class,
            'action'=>'actionCheckAccess'
        ],
        [
            'method'=>'POST',
            'pattern'=>'v1/kos>',
            'controller'=>\web\v1\main\MainController::class,
            'action'=>'editKos'
        ],

        [
            'method'=>'GET',
            'pattern'=>'v1/kos>',
            'controller'=>\web\v1\main\MainController::class,
            'action'=>'editKos'
        ],
        [
            'method'=>'POST',
            'pattern'=>'v1/reception>',
            'controller'=>\web\v1\main\MainController::class,
            'action'=>'actionReception'
        ],

        [
            'method'=>'GET',
            'pattern'=>'v1/reception>',
            'controller'=>\web\v1\main\MainController::class,
            'action'=>'actionReception'
        ],

        [
            'method'=>'GET',
            'pattern'=>'v1/ts>',
            'controller'=>\web\v1\main\MainController::class,
            'action'=>'actionTeamspeak'
        ],





    ]
];
