<?php

namespace web\v1\Repositories;

use Demodeos\Api\Classes\AbstractRepository;
use Demodeos\Api\Core;

class AccessTempRepository extends AbstractRepository
{
    const ROLES = [
        '241180'=>'5',
        '241181'=>'6',
        '241182'=>'7',
        '241183'=>'8',
        '241184'=>'9',
        '241185'=>'10',
    ];
    const ROLES_ID = [
        '5'=>'Рядовой',
        '6'=>'Капитан',
        '7'=>'Майор',
        '8'=>'Дипломат',
        '9'=>'Маршал',
        '10'=>'Мастер',
    ];
    const ROLES_NAME = [
        'Рядовой'=>'5',
        'Капитан'=>'6',
        'Майор'=>'7',
        'Дипломат'=>'8',
        'Маршал'=>'9',
        'Мастер'=>'10',
    ];


    public function checkAccessToken()
    {

        $access = Core::$app->headers()->get('access-token');

        if(!$access)
        {
            $access = Core::$app->request()->get('access-token');
        }
        $cookie = Core::$app->cookies()->get('XHFT');


        if($access && isset(self::ROLES[$access]))
        {
            Core::$app->cookies()->set('XHFT', $access);

            return ['role'=>self::ROLES[$access], 'user'=>self::ROLES_ID[self::ROLES[$access]]];

        }
        elseif($cookie && isset(self::ROLES[$cookie]))
        {


            return ['role'=>self::ROLES[$cookie], 'user'=>self::ROLES_ID[self::ROLES[$cookie]]];

        }

        else
        {
            return false;
        }


    }
    public function getAccessByNameMinimal(string $name)
    {
        $current_role = $this->checkAccessToken()['role'];

        if($current_role)
        {
            $role = self::ROLES_NAME[$name];

            if($current_role >= $role)
                return true;
            else
                return false;
        }
        return false;

    }

}