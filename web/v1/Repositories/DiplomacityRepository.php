<?php

namespace web\v1\Repositories;

use Demodeos\Api\Classes\AbstractRepository;
use Demodeos\Api\Core;
use Demodeos\DB\QueryBuilder\QueryBuilder;
use web\v1\models\kos;

class DiplomacityRepository extends AbstractRepository
{
    public $table_kos = 'kos';


    public function action()
    {
        $post = Core::$app->request()->post();


        if(Core::$app->request()->isPost() && AccessTempRepository::init()->checkAccessToken()) {
                return $this->create();
        }
        elseif(Core::$app->request()->isGet())
        {
            $result = $this->_sql->query('SELECT * FROM kos')->fetchAll(kos::class);


            return $result;


        }


    }

    public function create()
    {

        $data = json_decode(Core::$app->request()->body(), true);
        $query = QueryBuilder::insert($data)->table($this->table_kos)->onDuplicateKey()->create();

        $result = $this->_sql->query($query->sql, $query->params)->rows();

        return $result;

    }


}