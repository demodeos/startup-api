<?php

namespace web\v1\Repositories;

use Demodeos\Api\Classes\AbstractRepository;

use DateTime;
use DateInterval;


class Everydays extends AbstractRepository
{




    public function getEveryday()
    {
        $everydays = $this->getEverydayArray();
        $dates = $this->getDates();

        $today = $everydays[$dates['today']['year']][$dates['today']['month']][$dates['today']['day']];
        $tommorow = $everydays[$dates['tommorow']['year']][$dates['tommorow']['month']][$dates['tommorow']['day']];


        $everyday =[
            "date"=>date("d.m.Y"),
            'today'=>[
                "name"=>$everydays['env'][$today]['name'],
                "danges"=>$everydays['env'][$today]['danges'],

            ],
            'tommorow'=>[
                "name"=>$everydays['env'][$tommorow]['name'],
                "danges"=>$everydays['env'][$today]['danges'],

            ],
            "sever"=>$everydays['sever'][$dates['today']['day_count']]

        ];
        return $everyday;

    }


    public function getDates()
    {
        $now = new DateTime();
        $n = $this->parseDate($now);
        $tommorow = $now->add(DateInterval::createFromDateString("1 day"));
        $t = $this->parseDate($tommorow);
        $result = [
            "today"=>$n,
            "tommorow"=>$t,
        ];
        return $result;
    }
    public function parseDate(DateTime $date): array
    {
        $day = sprintf("%d", $date->format("d"));
        $month = sprintf("%d", $date->format("m"));
        $year = sprintf("%d", $date->format("Y"));
        $day_count = sprintf("%d", $date->format("w"));

        return compact("day","month","year", "day_count");

    }

    public function getEverydayArray()
    {
        $file = file_get_contents(FILES_DIR."json/everydays.json");

        $data = json_decode($file, true);

        return $data;
    }

}