<?php

namespace web\v1\Repositories;

use Demodeos\Api\Classes\AbstractRepository;
use Demodeos\Api\Core;
use Demodeos\DB\QueryBuilder\QueryBuilder;
use web\v1\models\reception;

class ReceptionRepository extends AbstractRepository
{

    public $table = 'reception';


    public function DeniedList()
    {


        $post = Core::$app->request()->post();


        if(Core::$app->request()->isPost() && AccessTempRepository::init()->checkAccessToken()) {
            return $this->saveDiniedList();
        }
        elseif(Core::$app->request()->isGet())
        {
            $result = $this->_sql->query('SELECT * FROM ' . $this->table)->fetchAll(reception::class);
            $key_val_array = [];
            foreach ($result as $value){

                $key_val_array[$value->nick] = $value;
            }
            ksort($key_val_array);

            return $key_val_array;


        }



    }

    public function saveDiniedList()
    {


        $data = json_decode(Core::$app->request()->body(), true);
        $query = QueryBuilder::insert($data)->table($this->table)->onDuplicateKey()->create();

        $result = $this->_sql->query($query->sql, $query->params)->rows();

        return $result;

    }


}