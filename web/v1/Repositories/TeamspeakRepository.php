<?php

namespace web\v1\Repositories;

use Cassandra\Date;
use TeamSpeak3;
use TeamSpeak3_Node_Host;
use TeamSpeak3_Node_Server;

class TeamspeakRepository
{
    private string $background = FILES_DIR.'/img/ts/background.jpg';
    private string $font = FILES_DIR.'/fonts/TimesNewRoman.ttf';
    private string $image_path = FILES_DIR.'/img/ts/';
    private string $image_url = 'https://startup-clan.ru/files/img/ts/';

    private static  ?TeamSpeak3_Node_Host $_connection = null;
    private static ?TeamSpeak3_Node_Server $_server = null;


    public function __construct($config)
    {
        $url = $config['url'];
        $login = $config['login'];
        $password = $config['password'];
        $port= $config['port'];

        if(is_null(static::$_connection))
        {
            @static::$_connection = TeamSpeak3::factory($url);
            static::$_connection->login($login, $password);
            static::$_server = @static::$_connection->serverGetByPort($port);
        }

    }

    public function createImage()
    {

        $everyday = Everydays::init()->getEveryday();
        $sever = explode(',',$everyday['sever']);
        $sever = array_map(fn($el)=>trim($el), $sever);

        $img = imagecreatefromjpeg($this->background);

        $color = imagecolorallocate($img, 255, 255, 255);
        $font = $this->font;



        ImageTTFtext($img, 25, 0, 2, 35, $color, $font, "ЕЖА ");
        ImageTTFtext($img, 10, 0, 50, 10, $color, $font, $everyday['date']);
        ImageTTFtext($img, 12, 0, 85, 22, $color, $font, 'Сегодня: '.$everyday['today']['name']);
        ImageTTFtext($img, 12, 0, 85, 35, $color, $font, 'Завтра: '.$everyday['tommorow']['name']);



        //ImageTTFtext($img, 12, 0, 85, 35, $color, $font, $this->sever[0]);
        ImageTTFtext($img, 8, 0, 250, 8, $color, $font, 'Север');
        if(isset($sever[0])) ImageTTFtext($img, 8, 0, 240, 20, $color, $font, $sever[0]);
        if(isset($sever[1])) ImageTTFtext($img, 8, 0, 270, 20, $color, $font, $sever[1]);

        if(isset($sever[2])) ImageTTFtext($img, 8, 0, 240, 30, $color, $font, $sever[2]);
        if(isset($sever[3])) ImageTTFtext($img, 8, 0, 270, 30, $color, $font, $sever[3]);


        /*
        header ("Content-type: image/png");
        ImagePng ($img);
        exit;
*/


        ImageTTFtext($img, 8, 0, 0, 45, $color, $font, 'Ежи в режиме тестирования');
        //  ImageTTFtext($img, 8, 0, 0, 45, $color, $font, $string1);
        // ImageTTFtext($img, 8, 0, 0, 55, $color, $font, $string2);
        //  ImageTTFtext($img, 8, 0, 0, 65, $color, $font, $string3);
        //  ImageTTFtext($img, 8, 0, 0, 75, $color, $font, $string4);
        //    ImageTTFtext($img, 8, 0, 0, 85, $color, $font, $string5);
        //   ImageTTFtext($img, 8, 0, 0, 95, $color, $font, 'Актуально на: '.$donn['time']);


        $everyday_filename = 'everyday'.time().'.jpg';

        $data = scandir($this->image_path);

        foreach ($data as $file)
        {
            if($file != "." && $file != ".." && $file != "background.jpg")
                unlink($this->image_path.$file);
        }

        imagejpeg($img, $this->image_path.$everyday_filename, 100);

        imagedestroy($img);

        return $this->image_url.$everyday_filename;
    }

    public  function setBanner(){
        $banner = $this->createImage();
        //static::$_server->channelGetByName('Дипломатия')->channel_description = '123213'
        $kos = DiplomacityRepository::init()->action();
        $reception = ReceptionRepository::init()->DeniedList();

        $key_val_array = [];
        foreach ($reception as $value){

            $key_val_array[$value->nick] = $value;
        }
        ksort($key_val_array);

        $string = 'КОС-ЛИСТ:'.PHP_EOL;
        $string_rec = '[size=large][color=blue]Запрет на прием:'.PHP_EOL;

        $color = '';
        $arr_kos = [];
        $arr_rec = [];

        foreach ($key_val_array as $item)
        {

            $arr_rec[] = "" . $item->nick . '(' . $item->class . ') - '. $item->comment.PHP_EOL ;
        }

        $string_rec .= implode('', $arr_rec);

        $string_rec .='[/color][/size]';

        foreach ($kos as $item)
        {
            if($item->status == 'Активный')
                $color = 'red';
            elseif ($item->status == 'Пассивный')
                $color= 'orange';
            elseif ($item->status == 'На рассмотрении')
                $color= 'yellow';

            $arr_kos[] = "[size=large][color=$color]" . $item->nick . '(' . $item->klass . ') [/color][/size]'.PHP_EOL ;
        }

        $string .= implode(' ', $arr_kos);

        static::$_server->channelGetByName('Дипломатия')->channel_description = $string;
        static::$_server->channelGetByName('Совет офицеров')->channel_description = $string_rec;







        @static::$_server->virtualserver_hostbanner_gfx_url = $banner;

    }


}
